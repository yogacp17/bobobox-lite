package com.bobobox.lite.presentation.dialogfragment.hotels.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bobobox.lite.domain.base.viewmodel.BaseViewModel
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.domain.room.database.DatabaseHelper
import com.bobobox.lite.domain.room.hotel.Hotel
import kotlinx.coroutines.launch
import org.koin.core.inject

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
class HotelDialogViewModel (application: Application): BaseViewModel(application) {

    private val database: DatabaseHelper by inject()

    private val _hotels = MutableLiveData<List<Hotel>>()
    val hotels: LiveData<List<Hotel>> get() = _hotels

    init {
        fetchHotelsData()
    }

    private fun fetchHotelsData() {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                _hotels.value = database.getAllHotels()
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }
}