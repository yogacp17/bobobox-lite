package com.bobobox.lite.presentation.dialogfragment.qrcode.viewmodel

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bobobox.lite.domain.base.viewmodel.BaseViewModel
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.external.extension.generateQrCode
import kotlinx.coroutines.launch

/**
 * Created by Yoga C. Pranata on 18/10/2020.
 * Android Engineer
 */
class QrCodeDialogViewModel (application: Application): BaseViewModel(application) {

    private val _generatedQrCode = MutableLiveData<Bitmap>()
    val generatedQrCode: LiveData<Bitmap> get() = _generatedQrCode

    fun generatehQrCode(qrCode: Bitmap?) {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                _generatedQrCode.value = qrCode
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

}