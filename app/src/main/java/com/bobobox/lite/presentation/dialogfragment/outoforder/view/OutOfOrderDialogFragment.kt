package com.bobobox.lite.presentation.dialogfragment.outoforder.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bobobox.lite.R
import com.bobobox.lite.databinding.DialogOutOfOrderBinding
import com.bobobox.lite.domain.base.dialogfragment.BaseDialogFragment
import com.bobobox.lite.domain.callback.DialogFragmentCallback
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.external.constant.AppConstant
import com.bobobox.lite.external.extension.notNull
import com.bobobox.lite.external.extension.setCustomWidthPopupDialog
import com.bobobox.lite.external.extension.setVisibleIf
import com.bobobox.lite.presentation.dialogfragment.outoforder.viewmodel.OutOfOrderViewModel
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Yoga C. Pranata on 18/10/2020.
 * Android Engineer
 */
class OutOfOrderDialogFragment : BaseDialogFragment() {

    private val viewModel: OutOfOrderViewModel by viewModel()
    lateinit var binding: DialogOutOfOrderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle90)
        isCancelable = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogOutOfOrderBinding.inflate(LayoutInflater.from(context))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLoadingState()
        setupButtonListener()
        initArguments()

        dialog?.setCustomWidthPopupDialog(AppConstant.POPUP_DIALOG_WIDTH)
    }

    private fun initArguments() {
        val data = arguments
        data.notNull {
            val roomId =it.getInt(AppConstant.INTENT_BUNDLE_KEY.ROOMID)
            viewModel._roomId.value = roomId
        }
    }

    private fun observeLoadingState() {
        viewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.RUNNING -> showLoading(true)
                LoadingState.Status.SUCCESS -> {
                    showLoading(false)
                    closeWithResult()
                }
                LoadingState.Status.FAILED -> {
                    showLoading(false)
                    showMessage(it.message)
                }
            }
        })
    }

    private fun setupButtonListener() {
        binding.btnCancel.setOnClickListener {
            dismissDialog()
        }

        binding.layoutReason.cbME01.setOnCheckedChangeListener { buttonView, _ ->
            viewModel.setSelectedReason(buttonView.text.toString())
        }

        binding.layoutReason.cbME02.setOnCheckedChangeListener { buttonView, _ ->
            viewModel.setSelectedReason(buttonView.text.toString())
        }

        binding.layoutReason.cbME03.setOnCheckedChangeListener { buttonView, _ ->
            viewModel.setSelectedReason(buttonView.text.toString())
        }

        binding.layoutReason.cbME04.setOnCheckedChangeListener { buttonView, _ ->
            viewModel.setSelectedReason(buttonView.text.toString())
        }

        binding.layoutReason.cbME05.setOnCheckedChangeListener { buttonView, _ ->
            viewModel.setSelectedReason(buttonView.text.toString())
        }

        binding.layoutReason.cbME06.setOnCheckedChangeListener { buttonView, _ ->
            viewModel.setSelectedReason(buttonView.text.toString())
        }

        binding.layoutReason.cbME99.setOnCheckedChangeListener { buttonView, _ ->
            viewModel.setSelectedReason(buttonView.text.toString())
        }

        binding.btnContinue.setOnClickListener {
            val description = binding.layoutReason.etOOOReason.text.toString()
            viewModel.saveData(description)
        }
    }

    private fun showLoading(isShow: Boolean) {
        binding.loadingView.setVisibleIf(isShow)
        binding.btnCancel.setVisibleIf(!isShow)
        binding.btnContinue.setVisibleIf(!isShow)
    }

    private fun closeWithResult() {
        (activity as DialogFragmentCallback).onResultDialog(AppConstant.DB_UPDATED)
        dismissDialog()
    }
}