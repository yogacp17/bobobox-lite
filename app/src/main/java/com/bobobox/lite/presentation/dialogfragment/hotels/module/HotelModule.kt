package com.bobobox.lite.presentation.dialogfragment.hotels.module

import com.bobobox.lite.presentation.dialogfragment.hotels.viewmodel.HotelDialogViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
val hotelModule = module {
    viewModel { HotelDialogViewModel(androidApplication()) }
}