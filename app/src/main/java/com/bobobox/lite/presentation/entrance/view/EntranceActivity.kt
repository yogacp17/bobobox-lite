package com.bobobox.lite.presentation.entrance.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.Observer
import com.bobobox.lite.databinding.ActivityEntranceBinding
import com.bobobox.lite.domain.base.view.BaseActivity
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.external.constant.AppConstant
import com.bobobox.lite.external.extension.setVisibleIf
import com.bobobox.lite.external.extension.viewbinding.viewBinding
import com.bobobox.lite.presentation.entrance.viewmodel.EntranceViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class EntranceActivity : BaseActivity() {

    private val viewModel: EntranceViewModel by viewModel()
    private val binding: ActivityEntranceBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        requestTransition()
        super.onCreate(savedInstanceState)
        setLoadingState()
        checkUserLogin()
        updateProgress(AppConstant.PROGRESSBAR.START)
    }

    private fun setLoadingState() {
        viewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.RUNNING -> showLoading(true)
                LoadingState.Status.SUCCESS -> {
                    updateProgress(AppConstant.PROGRESSBAR.MIDDLE)
                }
                LoadingState.Status.FAILED -> {
                    showLoading(false)
                    showMessage(it.message)
                }
            }
        })
    }

    private fun checkUserLogin() {
        viewModel.userLogin.observe(this, Observer {
            runSplashScreen(it)
        })
    }

    private fun showLoading(isShow: Boolean) {
        binding.loadingView.setVisibleIf(isShow)
        binding.textLoading.setVisibleIf(isShow)
    }

    private fun updateProgress(progress: Float) {
        binding.loadingView.progress = progress
    }

    private fun runSplashScreen(userLogin: String) {
        Handler(Looper.getMainLooper()).postDelayed({
            kotlin.run {
                updateProgress(AppConstant.PROGRESSBAR.FINISH)
                redirectUser(userLogin)
            }
        }, AppConstant.SPLASH_DELAY.toLong())
    }

    private fun redirectUser(userLogin: String) {
        if (userLogin.isNotEmpty()) {
            showLoading(false)
            router.goToHomepage(this)
        } else {
            showLoading(false)
            router.goToLogin(this)
        }
    }
}