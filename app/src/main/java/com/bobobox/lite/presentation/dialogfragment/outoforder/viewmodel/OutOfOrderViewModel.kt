package com.bobobox.lite.presentation.dialogfragment.outoforder.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bobobox.lite.data.model.pod.RoomNote
import com.bobobox.lite.domain.base.viewmodel.BaseViewModel
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.domain.room.database.DatabaseHelper
import com.bobobox.lite.external.constant.AppConstant
import com.bobobox.lite.external.extension.getNoteId
import com.bobobox.lite.external.extension.getRoomStatus
import com.bobobox.lite.external.extension.notNull
import com.bobobox.lite.external.extension.notNullOrEmpty
import kotlinx.coroutines.launch
import org.koin.core.inject

/**
 * Created by Yoga C. Pranata on 18/10/2020.
 * Android Engineer
 */
class OutOfOrderViewModel (application: Application) : BaseViewModel(application) {

    private val database: DatabaseHelper by inject()

    val _roomId = MutableLiveData<Int>()
    val roomId: LiveData<Int> get() = _roomId

    val _selectedReasons = MutableLiveData<List<RoomNote>>()
    val selectedReasons: LiveData<List<RoomNote>> get() = _selectedReasons

    fun saveData(description: String) {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING

                roomId.value.notNull {
                    val pod = database.getPod(it)
                    val roomNotes = pod.roomNotes.toMutableList()
                    val reasonList = selectedReasons.value?.toMutableList() ?: mutableListOf()

                    if(reasonList.isNotEmpty()) {
                        pod.roomNotes = roomNotes
                        pod.description = description
                        pod.roomStatusLabel = AppConstant.POD_STATUS.OOO
                        pod.roomStatusCode = AppConstant.POD_STATUS.OUD
                        database.updatePod(pod)
                    }
                }

                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

    fun setSelectedReason(reason: String) {
        var reasonList = selectedReasons.value?.toMutableList() ?: mutableListOf()

        if(reasonList.isNotEmpty()) {
            val isAlreadyAdded = reasonList.indexOfFirst { it.noteCode == reason.split(" ")[0] }
            if(isAlreadyAdded > AppConstant.ITEM_NOT_FOUND) {
                reasonList.removeAt(isAlreadyAdded)
            } else {
                reasonList = saveToRoomNoteList(reasonList, reason)
            }
        } else {
            reasonList = saveToRoomNoteList(reasonList, reason)
        }

        _selectedReasons.value = reasonList
    }

    private fun saveToRoomNoteList(reasonList: MutableList<RoomNote>, reason: String): MutableList<RoomNote> {
        val splitNote = reason.trim().split(' ')
        val noteCode = splitNote[0]
        val noteName = splitNote[1]
        val noteId = noteCode.getNoteId()
        val note = RoomNote(noteId, noteCode, noteName)
        reasonList.add(note)
        return reasonList
    }
}