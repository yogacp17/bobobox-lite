package com.bobobox.lite.presentation.homepage.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.Observer
import com.bobobox.lite.R
import com.bobobox.lite.databinding.ActivityHomepageBinding
import com.bobobox.lite.domain.base.view.BaseActivity
import com.bobobox.lite.domain.callback.DialogFragmentCallback
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.domain.room.hotel.Hotel
import com.bobobox.lite.domain.room.pod.Pod
import com.bobobox.lite.external.constant.AppConstant
import com.bobobox.lite.external.extension.*
import com.bobobox.lite.external.extension.viewbinding.viewBinding
import com.bobobox.lite.presentation.homepage.viewmodel.HomepageViewModel
import kotlinx.android.synthetic.main.item_pod_list.view.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
class HomepageActivity : BaseActivity(), DialogFragmentCallback {

    private val binding: ActivityHomepageBinding by viewBinding()
    private val viewModel: HomepageViewModel by viewModel()
    private var isGridListOpen = false

    override fun onCreate(savedInstanceState: Bundle?) {
        requestTransition()
        super.onCreate(savedInstanceState)
        observeLoadingState()
        observeSelectedHotel()
        observeTotalPods()
        observeSelectedPods()
        observeSelectedStatus()
        showLoading(true)
        setLogoutButton()
        setHotelButton()
        setWelcomeTitle()
        setButtonStatus()
        setButtonGrid()
        setupGridMenuClickListener()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    override fun <T> onResultDialog(data: T) {
        when (data) {
            is Hotel -> setSelectedHotel(data)
            is String -> {
                if(data == AppConstant.DB_UPDATED) {
                    val selectedStatus = viewModel.selectedStatus.value
                    selectedStatus.notNullOrEmpty { viewModel.getPodsFromStatus(it) }
                } else {
                    isGridListOpen = true
                    setSelectedStatus(data)
                }
            }
        }
    }

    private fun observeSelectedStatus() {
        viewModel.selectedStatus.observe(this, Observer {
            val selectedHotel = viewModel.selectedHotel.value
            selectedHotel.notNull { viewModel.fetchTotalPods(it) }
        })
    }

    private fun observeLoadingState() {
        viewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.RUNNING -> showLoading(true)
                LoadingState.Status.SUCCESS -> showLoading(false)
                LoadingState.Status.FAILED -> {
                    showLoading(false)
                    showMessage(it.message)
                }
            }
        })
    }

    private fun observeSelectedHotel() {
        viewModel.selectedHotel.observe(this, Observer {
            binding.selectedHotelTitle.text = it.name
            viewModel.fetchTotalPods(it)

            if (isGridListOpen) {
                setSelectedStatus(AppConstant.ALL_STATUS)
            }
        })
    }

    private fun observeTotalPods() {
        viewModel.totalVacantDirty.observe(this, Observer {
            binding.layoutGridMenu.tvTotalVD.text = it.toString()
        })

        viewModel.totalVacantCleaned.observe(this, Observer {
            binding.layoutGridMenu.tvTotalVC.text = it.toString()
        })

        viewModel.totalVacantCleanInspected.observe(this, Observer {
            binding.layoutGridMenu.tvTotalVCI.text = it.toString()
        })

        viewModel.totalOccupied.observe(this, Observer {
            binding.layoutGridMenu.tvTotalOccupied.text = it.toString()
        })

        viewModel.totalOutOfOrder.observe(this, Observer {
            binding.layoutGridMenu.tvTotalOOO.text = it.toString()
        })
    }

    private fun observeSelectedPods() {
        viewModel.selectedPods.observe(this, Observer {
            if (isGridListOpen) {
                setupPodList(it)
                showGridList(true)
                showGridButton(false)
            }
        })
    }

    private fun setWelcomeTitle() {
        binding.welcomeTitle.text = getString(R.string.welcome_title).format(
            utility.getTimeMessage(),
            appSession.getUserLogin()
        )
    }

    private fun setLogoutButton() {
        binding.btnLogout.setOnClickListener {
            viewModel.logoutUser()
            router.logout(this)
        }
    }

    private fun setHotelButton() {
        binding.layoutBtnHotel.setOnClickListener {
            router.openHotelDialog(this)
        }
    }

    private fun setSelectedHotel(hotel: Hotel) {
        viewModel._selectedHotel.value = hotel
        binding.selectedHotelTitle.text = hotel.name
    }

    private fun setButtonStatus() {
        val status = resources.getStringArray(R.array.statuses).toList().first()
        setSelectedStatus(status)
        binding.layoutTabSection.layoutBtnStatus.setOnClickListener {
            router.openAllStatusDialog(this)
        }
    }

    private fun setSelectedStatus(status: String) {
        binding.layoutTabSection.tvStatus.text = status
        viewModel.getPodsFromStatus(status)
    }

    private fun setButtonGrid() {
        binding.layoutTabSection.btnGridList.setOnClickListener {
            showGridButton(false)
            showGridList(true)
            setSelectedStatus(AppConstant.ALL_STATUS)
        }

        binding.layoutTabSection.btnGrid.setOnClickListener {
            showGridButton(true)
            showGridList(false)
            setSelectedStatus(AppConstant.ALL_STATUS)
        }
    }

    private fun setupGridMenuClickListener() {
        binding.layoutGridMenu.cardVacantDirty.setOnClickListener {
            setSelectedStatus(AppConstant.POD_STATUS.VD)
            showGridList(true)
            showGridButton(false)
        }

        binding.layoutGridMenu.cardVacantCleaned.setOnClickListener {
            setSelectedStatus(AppConstant.POD_STATUS.VC)
            showGridList(true)
            showGridButton(false)
        }

        binding.layoutGridMenu.cardVacantCleanInspected.setOnClickListener {
            setSelectedStatus(AppConstant.POD_STATUS.VCI)
            showGridList(true)
            showGridButton(false)
        }

        binding.layoutGridMenu.cardOccupied.setOnClickListener {
            setSelectedStatus(AppConstant.POD_STATUS.O)
            showGridList(true)
            showGridButton(false)
        }

        binding.layoutGridMenu.cardOutOfOrder.setOnClickListener {
            setSelectedStatus(AppConstant.POD_STATUS.OOO)
            showGridList(true)
            showGridButton(false)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupPodList(pods: List<Pod>) {
        binding.layoutPodList.recyclerViewPods.setup(
            pods,
            R.layout.item_pod_list,
            {
                tvPodName.text = getString(R.string.pods_number).format(it.roomName)
                it.roomStatusLabel.getRoomStatus().notNullOrEmpty { status ->
                    tvPodLabel.text = status
                    tvPodLabel.setVisibleIf(true)
                    tvPodLabel.getRoomStatus(it.roomStatusLabel)
                }
            },
            {
                //TODO: Add Swipe feature,
                // Temporary add it from click function
                when(it.roomStatusLabel) {
                    AppConstant.POD_STATUS.VC -> router.openOOOReasonDialog(this@HomepageActivity, it.roomId)
                    AppConstant.POD_STATUS.OOO -> router.openQrCodeDialog(this@HomepageActivity, it.qrCode, it.roomName)
                }
            }
        )
    }

    private fun showGridButton(isShow: Boolean) {
        binding.layoutTabSection.btnGridList.setActive(isShow)
        binding.layoutTabSection.btnGrid.setActive(!isShow)
    }

    private fun showGridList(isShow: Boolean) {
        isGridListOpen = isShow
        binding.layoutPodList.podList.setVisibleIf(isShow)
        binding.layoutGridMenu.podGridMenu.setVisibleIf(!isShow)
    }

    private fun showLoading(isShow: Boolean) {
        binding.layoutProgressbar.setVisibleIf(isShow)
    }
}