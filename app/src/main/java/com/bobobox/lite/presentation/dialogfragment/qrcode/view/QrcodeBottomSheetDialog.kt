package com.bobobox.lite.presentation.dialogfragment.qrcode.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bobobox.lite.R
import com.bobobox.lite.databinding.BottomsheetQrcodeBinding
import com.bobobox.lite.domain.base.dialogfragment.BaseBottomSheetDialogFragment
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.external.constant.AppConstant
import com.bobobox.lite.external.extension.generateQrCode
import com.bobobox.lite.external.extension.notNull
import com.bobobox.lite.external.extension.notNullOrEmpty
import com.bobobox.lite.external.extension.setVisibleIf
import com.bobobox.lite.presentation.dialogfragment.qrcode.viewmodel.QrCodeDialogViewModel
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Yoga C. Pranata on 18/10/2020.
 * Android Engineer
 */
class QrcodeBottomSheetDialog : BaseBottomSheetDialogFragment() {

    private val viewModel: QrCodeDialogViewModel by viewModel()
    lateinit var binding: BottomsheetQrcodeBinding

    override fun getHeightDialog(): Int = resources.getDimension(R.dimen.space_450).toInt()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = BottomsheetQrcodeBinding.inflate(LayoutInflater.from(context))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeGeneratedQrCode()
        observeLoadingState()
        initArguments()
    }

    private fun observeGeneratedQrCode() {
        viewModel.generatedQrCode.observe(this, Observer {
            binding.imageQrCode.setImageBitmap(it)
        })
    }

    private fun observeLoadingState() {
        viewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.RUNNING -> showLoading(true)
                LoadingState.Status.SUCCESS -> {
                    showLoading(false)
                }
                LoadingState.Status.FAILED -> {
                    showLoading(false)
                    showMessage(it.message)
                }
            }
        })
    }

    private fun initArguments() {
        val data = arguments
        data.notNull { bundle ->
            val qrCode = bundle.getString(AppConstant.INTENT_BUNDLE_KEY.QRCODE)
            val podNumber = bundle.getString(AppConstant.INTENT_BUNDLE_KEY.POD_NUMBER)

            qrCode.notNullOrEmpty {
                val generatedQrCode = it.generateQrCode(context)
                viewModel.generatehQrCode(generatedQrCode)
                setupButtonAndPodNumber(podNumber)
            } ?: showMessage("Cannot Generate QR Code, Value is empty")
        }
    }

    private fun setupButtonAndPodNumber(podNumber: String?) {
        binding.tvPodNumber.text = getString(R.string.pods_number).format(podNumber)
        binding.btnReset.setOnClickListener {
            showMessage("Pod Resetted")
            dismissBottomSheet()
        }
    }

    private fun showLoading(isShow: Boolean) {
        binding.loadingView.setVisibleIf(isShow)
    }
}