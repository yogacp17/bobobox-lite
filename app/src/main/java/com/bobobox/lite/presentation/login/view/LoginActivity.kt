package com.bobobox.lite.presentation.login.view

import android.os.Bundle
import androidx.lifecycle.Observer
import com.bobobox.lite.R
import com.bobobox.lite.databinding.ActivityLoginBinding
import com.bobobox.lite.domain.base.view.BaseActivity
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.external.extension.setVisibleIf
import com.bobobox.lite.external.extension.viewbinding.viewBinding
import com.bobobox.lite.presentation.login.viewmodel.LoginViewModel
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class LoginActivity : BaseActivity() {

    private val viewModel: LoginViewModel by viewModel()
    private val binding: ActivityLoginBinding by viewBinding()

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeLoadingState()
        observeUserLogin()
        setLoginButton()
    }

    private fun observeLoadingState() {
        viewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.RUNNING -> showLoading(true)
                LoadingState.Status.SUCCESS -> showLoading(false)
                LoadingState.Status.FAILED -> {
                    showLoading(false)
                    showMessage(it.message)
                }
            }
        })
    }

    private fun observeUserLogin() {
        viewModel.isUserLoggedIn.observe(this, Observer { isLoggedIn ->
            if (isLoggedIn) {
                router.goToHomepage(this)
            } else {
                showMessage(getString(R.string.login_error_message))
            }
        })
    }

    private fun setLoginButton() {
        binding.btnLogin.setOnClickListener {
            showLoading(true)

            val email = binding.emailEdittext.text.toString()
            val password = binding.passwordEdittext.text.toString()

            if (email.isNotEmpty() || password.isNotEmpty()) {
                viewModel.checkUserLogin(email, password)
            } else {
                showLoading(false)
                showMessage(getString(R.string.login_empty_message))
            }
        }
    }

    private fun showLoading(isShow: Boolean) {
        binding.loadingView.setVisibleIf(isShow)
        binding.btnLogin.setVisibleIf(!isShow)
    }
}