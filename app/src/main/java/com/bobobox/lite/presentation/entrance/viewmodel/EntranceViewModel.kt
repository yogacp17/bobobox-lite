package com.bobobox.lite.presentation.entrance.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bobobox.lite.domain.base.viewmodel.BaseViewModel
import com.bobobox.lite.domain.loading.LoadingState
import kotlinx.coroutines.launch

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class EntranceViewModel (application: Application) : BaseViewModel(application) {

    private var _userLogin = MutableLiveData<String>()
    val userLogin: LiveData<String>
        get() = _userLogin

    init {
        fetchUserLoggedin()
    }

    private fun fetchUserLoggedin() {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                checkUserLoggedIn()
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

    private fun checkUserLoggedIn() {
        if(appSession.isUserLoggedIn()) {
            _userLogin.value = appSession.getUserLogin()
        } else {
            _userLogin.value = ""
        }
    }
}