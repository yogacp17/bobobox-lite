package com.bobobox.lite.presentation.dialogfragment.qrcode.module

import com.bobobox.lite.presentation.dialogfragment.qrcode.viewmodel.QrCodeDialogViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yoga C. Pranata on 18/10/2020.
 * Android Engineer
 */
val qrCodeModule = module {
    viewModel { QrCodeDialogViewModel(androidApplication()) }
}