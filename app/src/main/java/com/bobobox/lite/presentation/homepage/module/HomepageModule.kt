package com.bobobox.lite.presentation.homepage.module

import com.bobobox.lite.presentation.homepage.viewmodel.HomepageViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
val homepageModule = module {
    viewModel { HomepageViewModel(androidApplication()) }
}