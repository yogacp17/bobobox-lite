package com.bobobox.lite.presentation.entrance.module

import com.bobobox.lite.presentation.entrance.viewmodel.EntranceViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
val entranceModule = module {
    viewModel { EntranceViewModel(androidApplication()) }
}