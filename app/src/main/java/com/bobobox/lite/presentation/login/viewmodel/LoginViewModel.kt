package com.bobobox.lite.presentation.login.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bobobox.lite.domain.base.viewmodel.BaseViewModel
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.external.constant.AppConstant
import kotlinx.coroutines.launch

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class LoginViewModel (application: Application) : BaseViewModel(application) {

    private var _isUserLoggedIn = MutableLiveData<Boolean>()
    val isUserLoggedIn: LiveData<Boolean>
        get() = _isUserLoggedIn

    fun checkUserLogin(email: String, password: String) {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING

                if(email == AppConstant.USER_LOGIN.email && password == AppConstant.USER_LOGIN.password) {
                    _isUserLoggedIn.value = true
                    appSession.saveUserLogin(AppConstant.USER_LOGIN.name)
                } else {
                    _isUserLoggedIn.value = false
                }

                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

}