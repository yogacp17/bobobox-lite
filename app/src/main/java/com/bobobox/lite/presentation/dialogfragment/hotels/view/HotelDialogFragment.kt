package com.bobobox.lite.presentation.dialogfragment.hotels.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bobobox.lite.R
import com.bobobox.lite.databinding.BottomsheetDialogBinding
import com.bobobox.lite.domain.base.dialogfragment.BaseBottomSheetDialogFragment
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.domain.room.hotel.Hotel
import com.bobobox.lite.external.extension.setVisibleIf
import com.bobobox.lite.external.extension.setup
import com.bobobox.lite.domain.callback.DialogFragmentCallback
import com.bobobox.lite.presentation.dialogfragment.hotels.viewmodel.HotelDialogViewModel
import kotlinx.android.synthetic.main.item_view_dialog.view.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
class HotelDialogFragment : BaseBottomSheetDialogFragment() {

    private val viewModel: HotelDialogViewModel by viewModel()
    lateinit var binding: BottomsheetDialogBinding

    override fun getHeightDialog(): Int = resources.getDimension(R.dimen.space_350).toInt()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = BottomsheetDialogBinding.inflate(LayoutInflater.from(context))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLoadingState()
        observeHotels()
    }

    private fun observeLoadingState() {
        viewModel.loadingState.observe(this, Observer {
            when (it.status) {
                LoadingState.Status.RUNNING -> showLoading(true)
                LoadingState.Status.SUCCESS -> showLoading(false)
                LoadingState.Status.FAILED -> {
                    showLoading(false)
                    showMessage(it.message)
                }
            }
        })
    }

    private fun observeHotels() {
        viewModel.hotels.observe(this, Observer {
            setRecyclerViewStatus(it)
        })
    }

    private fun showLoading(isShow: Boolean) {
        binding.loadingView.setVisibleIf(isShow)
        binding.recyclerView.setVisibleIf(!isShow)
    }

    private fun setRecyclerViewStatus(hotels: List<Hotel>) {
        binding.recyclerView.setup(
            hotels,
            R.layout.item_view_dialog,
            {
                tvTitle.text = it.name
            },
            {
                (activity as DialogFragmentCallback).onResultDialog(it)
                dismissBottomSheet()
            }
        )
    }
}