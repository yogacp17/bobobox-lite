package com.bobobox.lite.presentation.dialogfragment.allstatus.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bobobox.lite.R
import com.bobobox.lite.databinding.BottomsheetDialogBinding
import com.bobobox.lite.domain.base.dialogfragment.BaseBottomSheetDialogFragment
import com.bobobox.lite.external.extension.setup
import com.bobobox.lite.domain.callback.DialogFragmentCallback
import kotlinx.android.synthetic.main.item_view_dialog.view.*


/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
class AllStatusDialogFragment : BaseBottomSheetDialogFragment() {

    lateinit var binding: BottomsheetDialogBinding

    override fun getHeightDialog(): Int = resources.getDimension(R.dimen.space_250).toInt()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = BottomsheetDialogBinding.inflate(LayoutInflater.from(context))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerViewStatus()
    }

    private fun setRecyclerViewStatus() {
        val statuses = resources.getStringArray(R.array.statuses).toList()
        binding.recyclerView.setup(
            statuses,
            R.layout.item_view_dialog,
            {
                tvTitle.text = it
            },
            {
                (activity as DialogFragmentCallback).onResultDialog(it)
                dismissBottomSheet()
            }
        )
    }
}