package com.bobobox.lite.presentation.login.module

import com.bobobox.lite.presentation.login.viewmodel.LoginViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
val loginModule = module {
    viewModel { LoginViewModel(androidApplication()) }
}