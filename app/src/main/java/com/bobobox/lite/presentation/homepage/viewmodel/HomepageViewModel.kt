package com.bobobox.lite.presentation.homepage.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bobobox.lite.data.model.hotel.HotelResponse
import com.bobobox.lite.data.model.pod.PodResponse
import com.bobobox.lite.domain.base.viewmodel.BaseViewModel
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.domain.room.database.DatabaseHelper
import com.bobobox.lite.domain.room.hotel.Hotel
import com.bobobox.lite.domain.room.pod.Pod
import com.bobobox.lite.external.constant.AppConstant
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
class HomepageViewModel(application: Application) : BaseViewModel(application), KoinComponent {

    private val database: DatabaseHelper by inject()

    val _selectedHotel = MutableLiveData<Hotel>()
    val selectedHotel: LiveData<Hotel> get() = _selectedHotel

    private val _totalVacantDirty = MutableLiveData<Int>()
    val totalVacantDirty: LiveData<Int> get() = _totalVacantDirty

    private val _totalVacantCleaned = MutableLiveData<Int>()
    val totalVacantCleaned: LiveData<Int> get() = _totalVacantCleaned

    private val _totalVacantCleanInspected = MutableLiveData<Int>()
    val totalVacantCleanInspected: LiveData<Int> get() = _totalVacantCleanInspected

    private val _totalOccupied = MutableLiveData<Int>()
    val totalOccupied: LiveData<Int> get() = _totalOccupied

    private val _totalOutOfOrder = MutableLiveData<Int>()
    val totalOutOfOrder: LiveData<Int> get() = _totalOutOfOrder

    private val _selectedPods = MutableLiveData<List<Pod>>()
    val selectedPods: LiveData<List<Pod>> get() = _selectedPods

    val _selectedStatus = MutableLiveData<String>()
    val selectedStatus: LiveData<String> get() = _selectedStatus

    init {
        fetchData()
    }

    private fun fetchData() {
        fetchHotels()
        fetchPods()
    }

    private fun fetchHotels() {
        val inputStream = context.assets.open("response/hotel_list.json")
        val inputString = inputStream.bufferedReader().use { it.readText() }

        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING

                val hotelResponse = gson.fromJson(inputString, HotelResponse::class.java)
                val hotels = arrayListOf<com.bobobox.lite.domain.room.hotel.Hotel>()

                hotelResponse.data?.forEach { hotel ->
                    val dbHotel = com.bobobox.lite.domain.room.hotel.Hotel(
                        id = hotel.id,
                        latitude = hotel.latitude,
                        longitude = hotel.longitude,
                        address = hotel.address,
                        city = hotel.city,
                        email = hotel.email,
                        mqttTopic = hotel.mqttTopic,
                        name = hotel.name,
                        pictureUrl = hotel.pictureUrl,
                        rating = hotel.rating,
                        slug = hotel.slug,
                        telephone = hotel.telephone
                    )

                    hotels.add(dbHotel)
                }

                database.insertAllHotel(hotels)
                _selectedHotel.value = hotels.first()
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

    private fun fetchPods() {
        val inputStream = context.assets.open("response/pod_list.json")
        val inputString = inputStream.bufferedReader().use { it.readText() }

        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING

                val podResponse = gson.fromJson(inputString, PodResponse::class.java)
                val pods = arrayListOf<com.bobobox.lite.domain.room.pod.Pod>()

                podResponse.data?.forEach { pod ->
                    val dbPod = com.bobobox.lite.domain.room.pod.Pod(
                        roomId = pod.roomId,
                        roomName = pod.roomName,
                        roomNotes = pod.roomNotes,
                        roomStatus = pod.roomStatus,
                        roomStatusCode = pod.roomStatusCode,
                        roomStatusLabel = pod.roomStatusLabel,
                        roomType = pod.roomType,
                        roomTypeLabel = pod.roomTypeLabel,
                        description = pod.description,
                        mqttTopic = pod.mqttTopic,
                        qrCode = pod.qrCode
                    )

                    pods.add(dbPod)
                }

                database.insertAllPod(pods)
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

    fun fetchTotalPods(selectedHotel: Hotel) {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                _totalVacantDirty.value = database.getPodsWithStatus(AppConstant.POD_STATUS.VD, selectedHotel.id.toString()).size
                _totalVacantCleaned.value = database.getPodsWithStatus(AppConstant.POD_STATUS.VC, selectedHotel.id.toString()).size
                _totalVacantCleanInspected.value = database.getPodsWithStatus(AppConstant.POD_STATUS.VCI, selectedHotel.id.toString()).size
                _totalOccupied.value = database.getPodsWithStatus(AppConstant.POD_STATUS.O, selectedHotel.id.toString()).size
                _totalOutOfOrder.value = database.getPodsWithStatus(AppConstant.POD_STATUS.OOO, selectedHotel.id.toString()).size
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

    fun getPodsFromStatus(status: String) {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING

                _selectedPods.value = if(status == AppConstant.ALL_STATUS) {
                    database.getAllPods(selectedHotel.value?.id.toString())
                } else {
                    database.getPodsWithStatus(status, selectedHotel.value?.id.toString())
                }

                _selectedStatus.value = status
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }

    fun logoutUser() {
        viewModelScope.launch {
            try {
                _loadingState.value = LoadingState.LOADING
                database.deleteAllHotel()
                database.deleteAllPod()
                appSession.clear()
                _loadingState.value = LoadingState.LOADED
            } catch (error: Exception) {
                _loadingState.value = LoadingState.error(error.message)
            }
        }
    }
}