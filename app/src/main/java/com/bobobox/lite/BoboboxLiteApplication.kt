package com.bobobox.lite

import android.app.Application
import com.bobobox.lite.external.extension.debugMode
import com.bobobox.lite.koin.appComponents
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class BoboboxLiteApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initializeKoin()
    }

    private fun initializeKoin() {
        startKoin {
            debugMode { androidLogger(Level.ERROR) }
            androidContext(this@BoboboxLiteApplication)
            modules(appComponents)
        }
    }
}