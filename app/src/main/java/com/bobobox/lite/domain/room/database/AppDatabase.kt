package com.bobobox.lite.domain.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bobobox.lite.domain.room.hotel.Hotel
import com.bobobox.lite.domain.room.hotel.HotelDao
import com.bobobox.lite.domain.room.pod.Pod
import com.bobobox.lite.domain.room.pod.PodDao
import com.bobobox.lite.domain.room.typeconverter.RoomTypeConverters

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
@Database(version = 1, exportSchema = false, entities = [Hotel::class, Pod::class])
@TypeConverters(RoomTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun hotelDao(): HotelDao
    abstract fun podDao(): PodDao
}