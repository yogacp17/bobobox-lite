package com.bobobox.lite.domain.room.pod

import androidx.room.*

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
@Dao
interface PodDao {
    @Query("SELECT * FROM pod WHERE mqttTopic = :selectedHotel")
    suspend fun getAllPods(selectedHotel: String): List<Pod>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(pods: List<Pod>)

    @Delete
    suspend fun deletePod(pod: Pod)

    @Query("DELETE FROM pod")
    suspend fun deleteAllPod()

    @Query("SELECT * FROM pod WHERE roomStatusLabel = :status AND mqttTopic = :selectedHotel")
    suspend fun getPodsWithStatus(status: String, selectedHotel: String): List<Pod>

    @Query("SELECT * FROM pod WHERE roomId = :roomId")
    suspend fun getPod(roomId: Int): Pod

    @Update(entity = Pod::class)
    suspend fun updatePod(pod: Pod)
}