package com.bobobox.lite.domain.room.database

import com.bobobox.lite.domain.room.hotel.Hotel
import com.bobobox.lite.domain.room.pod.Pod

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class DatabaseHelperImpl(private val appDatabase: AppDatabase) : DatabaseHelper {

    /**
     * Hotel Database
     */
    override suspend fun getAllHotels(): List<Hotel> = appDatabase.hotelDao().getAllHotels()
    override suspend fun insertAllHotel(hotels: List<Hotel>) = appDatabase.hotelDao().insertAll(hotels)
    override suspend fun deleteHotel(hotel: Hotel) = appDatabase.hotelDao().deleteHotel(hotel)
    override suspend fun deleteAllHotel() = appDatabase.hotelDao().deleteAllHotel()


    /**
     * Pod Database
     */
    override suspend fun getAllPods(selectedHotel: String): List<Pod> = appDatabase.podDao().getAllPods(selectedHotel)
    override suspend fun getPod(roomId: Int): Pod = appDatabase.podDao().getPod(roomId)
    override suspend fun insertAllPod(pods: List<Pod>) = appDatabase.podDao().insertAll(pods)
    override suspend fun updatePod(pod: Pod) = appDatabase.podDao().updatePod(pod)
    override suspend fun deletePod(pod: Pod) = appDatabase.podDao().deletePod(pod)
    override suspend fun deleteAllPod() = appDatabase.podDao().deleteAllPod()
    override suspend fun getPodsWithStatus(status: String, selectedHotel: String): List<Pod> = appDatabase.podDao().getPodsWithStatus(status, selectedHotel)

}