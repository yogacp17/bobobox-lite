package com.bobobox.lite.domain.base.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bobobox.lite.domain.loading.LoadingState
import com.bobobox.lite.domain.session.AppSession
import com.google.gson.Gson
import org.koin.core.KoinComponent
import org.koin.core.inject

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
abstract class BaseViewModel(application: Application) : AndroidViewModel(application), KoinComponent {

    val appSession: AppSession by inject()
    val gson: Gson by inject()

    val context = application
    val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
        get() = _loadingState
}