package com.bobobox.lite.domain.room.hotel

import androidx.room.*

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
@Dao
interface HotelDao {
    @Query("SELECT * FROM hotel")
    suspend fun getAllHotels(): List<Hotel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(hotels: List<Hotel>)

    @Delete
    suspend fun deleteHotel(hotel: Hotel)

    @Query("DELETE FROM hotel")
    suspend fun deleteAllHotel()
}