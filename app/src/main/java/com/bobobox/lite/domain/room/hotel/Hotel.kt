package com.bobobox.lite.domain.room.hotel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
@Entity
data class Hotel(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "latitude") val latitude: String,
    @ColumnInfo(name = "longitude") val longitude: String,
    @ColumnInfo(name = "address") val address: String,
    @ColumnInfo(name = "city") val city: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "mqttTopic") val mqttTopic: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "pictureUrl") val pictureUrl: String,
    @ColumnInfo(name = "rating") val rating: String,
    @ColumnInfo(name = "slug") val slug: String,
    @ColumnInfo(name = "telephone") val telephone: String,
    @ColumnInfo(name = "hotel_status") val hotel_status: String = "",
    @ColumnInfo(name = "ooo_reason") val ooo_status: String = "",
    @ColumnInfo(name = "hotel_notes") val hotel_notes: String = ""
)