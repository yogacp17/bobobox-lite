package com.bobobox.lite.domain.room.database

import android.content.Context
import androidx.room.Room
import com.bobobox.lite.external.constant.AppConstant

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
object DatabaseBuilder {
    private var instance: AppDatabase? = null

    fun getInstance(context: Context): AppDatabase {
        return instance ?: synchronized(this) {
            instance ?: buildRoomDB(context).also { instance = it }
        }
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            AppConstant.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
}