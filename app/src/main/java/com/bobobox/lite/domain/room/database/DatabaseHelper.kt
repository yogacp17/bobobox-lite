package com.bobobox.lite.domain.room.database

import com.bobobox.lite.domain.room.hotel.Hotel
import com.bobobox.lite.domain.room.pod.Pod

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
interface DatabaseHelper {
    suspend fun getAllHotels(): List<Hotel>
    suspend fun insertAllHotel(hotels: List<Hotel>)
    suspend fun deleteHotel(hotel: Hotel)
    suspend fun deleteAllHotel()
    suspend fun getAllPods(selectedHotel: String): List<Pod>
    suspend fun getPodsWithStatus(status: String, selectedHotel: String): List<Pod>
    suspend fun getPod(roomId: Int): Pod
    suspend fun updatePod(pod: Pod)
    suspend fun insertAllPod(pods: List<Pod>)
    suspend fun deletePod(pod: Pod)
    suspend fun deleteAllPod()
}