package com.bobobox.lite.domain.router

import android.content.Context
import android.content.Intent
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
interface ScreenRouter {

    sealed class ActivityScreen {
        object Login : ActivityScreen()
        object Homepage : ActivityScreen()
    }

    sealed class DialogScreen {
        object AllStatus : DialogScreen()
        object Hotel : DialogScreen()
        object OOOReason : DialogScreen()
        object QrCode : DialogScreen()
    }

    fun getScreenIntent(context: Context, screen: ActivityScreen): Intent?
    fun getDialogFragmentScreenLayout(screen: DialogScreen): DialogFragment?
    fun getBottomSheetDialogFragmentScreenLayout(screen: DialogScreen): BottomSheetDialogFragment?
}