package com.bobobox.lite.domain.room.pod

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bobobox.lite.data.model.pod.RoomNote

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
@Entity
data class Pod(
    @PrimaryKey val roomId: Int,
    @ColumnInfo(name = "roomName") var roomName: String,
    @ColumnInfo(name = "roomStatus") var roomStatus: Int,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "mqttTopic") var mqttTopic: String,
    @ColumnInfo(name = "qrCode") var qrCode: String,
    @ColumnInfo(name = "roomNotes") var roomNotes: List<RoomNote>,
    @ColumnInfo(name = "roomStatusCode") var roomStatusCode: String,
    @ColumnInfo(name = "roomStatusLabel") var roomStatusLabel: String,
    @ColumnInfo(name = "roomType") var roomType: String,
    @ColumnInfo(name = "roomTypeLabel") var roomTypeLabel: String,
    @ColumnInfo(name = "pod_status") var pod_status: String = "",
    @ColumnInfo(name = "ooo_reason") var ooo_status: String = "",
    @ColumnInfo(name = "pod_notes") var pod_notes: String = ""
)