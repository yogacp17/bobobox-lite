package com.bobobox.lite.domain.router

import android.content.Context
import android.content.Intent
import androidx.fragment.app.DialogFragment
import com.bobobox.lite.presentation.dialogfragment.allstatus.view.AllStatusDialogFragment
import com.bobobox.lite.presentation.dialogfragment.hotels.view.HotelDialogFragment
import com.bobobox.lite.presentation.dialogfragment.outoforder.view.OutOfOrderDialogFragment
import com.bobobox.lite.presentation.dialogfragment.qrcode.view.QrcodeBottomSheetDialog
import com.bobobox.lite.presentation.entrance.view.EntranceActivity
import com.bobobox.lite.presentation.homepage.view.HomepageActivity
import com.bobobox.lite.presentation.login.view.LoginActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class ScreenRouterImpl : ScreenRouter {

    override fun getScreenIntent(context: Context, screen: ScreenRouter.ActivityScreen): Intent? {
        val klazz: Class<*>? = when(screen) {
            ScreenRouter.ActivityScreen.Login -> LoginActivity::class.java
            ScreenRouter.ActivityScreen.Homepage -> HomepageActivity::class.java
        }

        return if(klazz == null) null else Intent(context, klazz)
    }

    override fun getDialogFragmentScreenLayout(screen: ScreenRouter.DialogScreen): DialogFragment? {
        return when(screen) {
            ScreenRouter.DialogScreen.OOOReason -> OutOfOrderDialogFragment()
            else -> null
        }
    }

    override fun getBottomSheetDialogFragmentScreenLayout(screen: ScreenRouter.DialogScreen): BottomSheetDialogFragment? {
        return when(screen) {
            ScreenRouter.DialogScreen.AllStatus -> AllStatusDialogFragment()
            ScreenRouter.DialogScreen.Hotel -> HotelDialogFragment()
            ScreenRouter.DialogScreen.QrCode -> QrcodeBottomSheetDialog()
            else -> null
        }
    }

}