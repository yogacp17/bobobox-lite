package com.bobobox.lite.domain.session

import android.app.Application
import android.content.Context
import com.bobobox.lite.external.constant.AppConstant

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class AppSession constructor(application: Application) {

    val pref = Pref(application)

    inner class Pref internal constructor(val application: Application) : BaseSharedPreferences() {
        init {
            check()
        }

        override fun getApplicationContext(): Context {
            return application
        }

        override fun getSharedPrefName(): String {
            return AppConstant.SHARED_PREF.SHARED_NAME
        }
    }

    fun clear() {
        pref._clear()
    }

    fun clearUserLogin() {
        saveUserLogin("")
    }

    fun saveUserLogin(userLoginInfo: String) {
        pref._setString(AppConstant.SHARED_PREF.USER_LOGIN, userLoginInfo)
    }

    fun getUserLogin(): String? {
        return pref._getString(AppConstant.SHARED_PREF.USER_LOGIN, "")
    }

    fun isUserLoggedIn(): Boolean {
        return (getUserLogin()?.isNotEmpty() == true)
    }

}