package com.bobobox.lite.domain.base.dialogfragment

import android.widget.Toast
import androidx.fragment.app.DialogFragment

/**
 * Created by Yoga C. Pranata on 18/10/2020.
 * Android Engineer
 */
abstract class BaseDialogFragment : DialogFragment() {

    fun dismissDialog() {
        dismissAllowingStateLoss()
    }

    fun showMessage(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

}