package com.bobobox.lite.domain.router

import android.app.ActivityOptions
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.bobobox.lite.domain.room.hotel.Hotel
import com.bobobox.lite.external.constant.AppConstant
import com.bobobox.lite.external.constant.DialogConstant

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class RouterNavigation(private val screenRouter: ScreenRouter) {

    fun goToLogin(context: AppCompatActivity) {
        val screen = screenRouter.getScreenIntent(context, ScreenRouter.ActivityScreen.Login)
        screen?.run {
            context.startActivity(
                this,
                ActivityOptions.makeSceneTransitionAnimation(context).toBundle()
            )
        }
    }

    fun goToHomepage(context: AppCompatActivity) {
        val screen = screenRouter.getScreenIntent(context, ScreenRouter.ActivityScreen.Homepage)
        screen?.run { context.startActivity(this) }
    }

    fun logout(context: AppCompatActivity) {
        goToLogin(context)
    }

    fun openAllStatusDialog(context: AppCompatActivity) {
        val screen =
            screenRouter.getBottomSheetDialogFragmentScreenLayout(ScreenRouter.DialogScreen.AllStatus)
        screen?.show(context.supportFragmentManager, DialogConstant.DIALOG_SCREEN.ALL_STATUS)
    }

    fun openHotelDialog(context: AppCompatActivity) {
        val screen =
            screenRouter.getBottomSheetDialogFragmentScreenLayout(ScreenRouter.DialogScreen.Hotel)
        screen?.show(context.supportFragmentManager, DialogConstant.DIALOG_SCREEN.HOTELS)
    }

    fun openOOOReasonDialog(context: AppCompatActivity, roomId: Int) {
        val screen =
            screenRouter.getDialogFragmentScreenLayout(ScreenRouter.DialogScreen.OOOReason)
        val arguments = Bundle()
        arguments.putInt(AppConstant.INTENT_BUNDLE_KEY.ROOMID, roomId)
        screen?.arguments = arguments
        screen?.show(context.supportFragmentManager, DialogConstant.DIALOG_SCREEN.OOO_REASON)
    }

    fun openQrCodeDialog(context: AppCompatActivity, qrCode: String, podNumber: String) {
        val screen =
            screenRouter.getBottomSheetDialogFragmentScreenLayout(ScreenRouter.DialogScreen.QrCode)
        val arguments = Bundle()
        arguments.apply {
            putString(AppConstant.INTENT_BUNDLE_KEY.QRCODE, qrCode)
            putString(AppConstant.INTENT_BUNDLE_KEY.POD_NUMBER, podNumber)
        }
        screen?.arguments = arguments
        screen?.show(context.supportFragmentManager, DialogConstant.DIALOG_SCREEN.QR_CODE)
    }
}