package com.bobobox.lite.domain.callback

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
interface DialogFragmentCallback {
    fun <T> onResultDialog(data: T)
}