package com.bobobox.lite.domain.session

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
abstract class BaseSharedPreferences {

    protected var mSettings: SharedPreferences? = null
    protected abstract fun getApplicationContext(): Context

    protected open fun getSharedPrefName(): String {
        return ""
    }

    fun check() {
        if (mSettings == null) {
            val context = getApplicationContext()
            mSettings = context.getSharedPreferences(getSharedPrefName(), 0)
        }
    }

    fun _remove(tag: String) {
        val editor = mSettings!!.edit()
        editor.remove(tag)
        editor.apply()
    }

    fun _getString(tag: String, defaultStr: String): String? {
        return mSettings!!.getString(tag, defaultStr)
    }

    fun _setString(tag: String, valueStr: String) {
        val editor = mSettings!!.edit()
        editor.putString(tag, valueStr)
        editor.apply()
    }

    fun _setBoolean(tag: String, valueBoolean: Boolean) {
        val editor = mSettings!!.edit()
        editor.putBoolean(tag, valueBoolean)
        editor.apply()
    }

    fun _getBoolean(tag: String, defaultBoolean: Boolean): Boolean? {
        return mSettings!!.getBoolean(tag, defaultBoolean)
    }

    fun _getInt(tag: String, defaultInt: Int): Int? {
        return mSettings!!.getInt(tag, defaultInt)
    }

    fun _setInt(tag: String, valueInt: Int) {
        val editor = mSettings!!.edit()
        editor.putInt(tag, valueInt)
        editor.apply()
    }

    fun _clear() {
        val editor = mSettings!!.edit()
        editor.clear()
        editor.apply()
    }

}