package com.bobobox.lite.domain.room.typeconverter

import androidx.room.TypeConverter
import com.bobobox.lite.data.model.pod.RoomNote
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class RoomTypeConverters {

    @TypeConverter
    fun toStringArrayList(value: String): ArrayList<String> {
        val listType = object: TypeToken<ArrayList<String>>(){}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromStringArrayList(value: ArrayList<String>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun fromRoomNotes(value: List<RoomNote>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toRoomNotes(value: String): List<RoomNote> {
        val typeToken = object: TypeToken<List<RoomNote>>(){}.type
        return Gson().fromJson(value, typeToken)
    }
}