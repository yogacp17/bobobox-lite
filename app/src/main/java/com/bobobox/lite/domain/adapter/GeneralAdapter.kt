package com.bobobox.lite.domain.adapter

import android.view.View

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
class GeneralAdapter<ITEM>(
    items: List<ITEM>,
    layoutResId: Int,
    private val bindHolder: View.(ITEM) -> Unit
): AbstractAdapter<ITEM>(items, layoutResId) {

    private var itemClick: View.(ITEM) -> Unit = {}

    constructor(
        items: List<ITEM>,
        layoutResId: Int,
        bindHolder: View.(ITEM) -> Unit,
        itemViewClick: View.(ITEM) -> Unit = {}
    ): this(items, layoutResId, bindHolder) {
        this.itemClick = itemViewClick
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        super.onBindViewHolder(holder, position)
        if(position == holder.adapterPosition) {
            holder.itemView.bindHolder(itemList[position])
        }
    }

    override fun onItemClick(itemView: View, position: Int) {
        itemView.itemClick(itemList[position])
    }
}