package com.bobobox.lite.domain.base.view

import android.transition.Explode
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bobobox.lite.domain.router.RouterNavigation
import com.bobobox.lite.domain.session.AppSession
import com.bobobox.lite.external.utility.Utility
import org.koin.android.ext.android.inject

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
abstract class BaseActivity : AppCompatActivity() {

    val appSession: AppSession by inject()
    val router: RouterNavigation by inject()
    val utility: Utility by inject()

    fun showMessage(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun requestTransition() {
        with(window) {
            requestFeature(android.view.Window.FEATURE_CONTENT_TRANSITIONS)
            exitTransition = Explode()
        }
    }
}