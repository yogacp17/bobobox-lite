package com.bobobox.lite.domain.loading

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
data class LoadingState (val status: Status, val message: String? = null) {
    companion object {
        val LOADED = LoadingState(Status.SUCCESS)
        val LOADING = LoadingState(Status.RUNNING)
        fun error(msg: String?) = LoadingState(Status.FAILED, msg)
    }

    enum class Status {
        RUNNING,
        SUCCESS,
        FAILED
    }
}