package com.bobobox.lite.domain.base.dialogfragment

import android.app.Dialog
import android.os.Bundle
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.bobobox.lite.R
import com.bobobox.lite.external.extension.notNull
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    abstract fun getHeightDialog() : Int

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener { setupHeight(it as BottomSheetDialog, getHeightDialog()) }
        return dialog
    }

    private fun setupHeight(bottomSheetDialog: BottomSheetDialog, height: Int) {
        val frameLayout = bottomSheetDialog.findViewById<FrameLayout>(R.id.design_bottom_sheet)
        val behavior = frameLayout?.let { BottomSheetBehavior.from(it) }
        val layoutParams = frameLayout?.layoutParams

        layoutParams.notNull {
            it.height = height
        }

        frameLayout?.layoutParams = layoutParams
        frameLayout?.background =
            ResourcesCompat.getDrawable(resources, R.drawable.bg_bottomsheet_rounded, null)
        behavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun dismissBottomSheet() {
        dismissAllowingStateLoss()
    }

    fun showMessage(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}