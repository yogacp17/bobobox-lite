package com.bobobox.lite.koin

import com.bobobox.lite.presentation.dialogfragment.hotels.module.hotelModule
import com.bobobox.lite.presentation.dialogfragment.outoforder.module.outOfOrderModule
import com.bobobox.lite.presentation.dialogfragment.qrcode.module.qrCodeModule
import com.bobobox.lite.presentation.entrance.module.entranceModule
import com.bobobox.lite.presentation.homepage.module.homepageModule
import com.bobobox.lite.presentation.login.module.loginModule
import org.koin.core.module.Module

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
val appComponents: List<Module> = listOf(
    /**
     * Base Module
     */
    appModule,
    databaseModule,

    /**
     * Feature Module
     */
    entranceModule,
    loginModule,
    homepageModule,

    /**
     * DialogFragment Module
     */
    hotelModule,
    outOfOrderModule,
    qrCodeModule
)