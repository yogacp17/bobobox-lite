package com.bobobox.lite.koin

import com.bobobox.lite.domain.router.RouterNavigation
import com.bobobox.lite.domain.router.ScreenRouter
import com.bobobox.lite.domain.router.ScreenRouterImpl
import com.bobobox.lite.domain.session.AppSession
import com.bobobox.lite.external.utility.Utility
import com.google.gson.Gson
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
val appModule = module {
    single { Gson() }
    single { AppSession(androidApplication()) }
    single<ScreenRouter> { ScreenRouterImpl() }
    single { RouterNavigation(get()) }
    single { Utility() }
}