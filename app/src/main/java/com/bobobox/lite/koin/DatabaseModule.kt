package com.bobobox.lite.koin

import com.bobobox.lite.domain.room.database.DatabaseBuilder
import com.bobobox.lite.domain.room.database.DatabaseHelper
import com.bobobox.lite.domain.room.database.DatabaseHelperImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
val databaseModule = module {
    single<DatabaseHelper> { DatabaseHelperImpl(DatabaseBuilder.getInstance(androidApplication())) }
}