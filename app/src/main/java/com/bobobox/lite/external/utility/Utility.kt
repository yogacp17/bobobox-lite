package com.bobobox.lite.external.utility

import java.util.*


/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
class Utility {
    fun getTimeMessage(): String {
        val calendar = Calendar.getInstance()
        return when(calendar.get(Calendar.HOUR_OF_DAY)) {
            in 0..11 -> "Good Morning"
            in 12..15 -> "Good Afternoon"
            in 16..20 -> "Good Evening"
            in 21..24 -> "Good Night"
            else -> ""
        }
    }
}