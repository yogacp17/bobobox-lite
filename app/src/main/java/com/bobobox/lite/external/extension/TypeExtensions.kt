package com.bobobox.lite.external.extension

import android.app.Dialog
import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.view.Display
import android.view.Gravity
import android.view.WindowManager
import android.widget.TextView
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import com.bobobox.lite.BuildConfig
import com.bobobox.lite.R
import com.bobobox.lite.external.constant.AppConstant
import com.google.zxing.WriterException


/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
fun debugMode(function: () -> Unit) {
    if (BuildConfig.DEBUG) {
        function()
    }
}

inline fun <T : Any> T?.notNull(f: (it: T) -> Unit) {
    if (this != null) f(this)
}

inline fun String?.notNullOrEmpty(f: (it: String) -> Unit): String? {
    return if (this != null && this.isNotEmpty()) {
        f(this)
        this
    } else null
}

fun String?.getRoomStatus(): String {
    return when (this) {
        AppConstant.POD_STATUS.VD -> "VD"
        AppConstant.POD_STATUS.VC -> "VC"
        AppConstant.POD_STATUS.VCI -> "VCI"
        AppConstant.POD_STATUS.O -> "O"
        AppConstant.POD_STATUS.OOO -> "OOO"
        else -> ""
    }
}

fun TextView.getRoomStatus(status: String) {
    val color = when (status) {
        AppConstant.POD_STATUS.VD -> ContextCompat.getColor(context, R.color.dark_grey)
        AppConstant.POD_STATUS.VC -> ContextCompat.getColor(context, R.color.yellow)
        AppConstant.POD_STATUS.VCI -> ContextCompat.getColor(context, R.color.orange)
        AppConstant.POD_STATUS.O -> ContextCompat.getColor(context, R.color.teal_200)
        AppConstant.POD_STATUS.OOO -> ContextCompat.getColor(context, R.color.red)
        else -> null
    }

    color?.let { backgroundTintList = ColorStateList.valueOf(it) }
}

fun String.getNoteId() : Int {
    return when(this) {
        AppConstant.OOO_NOTE_CODE.ME01 -> AppConstant.OOO_NOTE_ID.ME01
        AppConstant.OOO_NOTE_CODE.ME02 -> AppConstant.OOO_NOTE_ID.ME02
        AppConstant.OOO_NOTE_CODE.ME03 -> AppConstant.OOO_NOTE_ID.ME03
        AppConstant.OOO_NOTE_CODE.ME04 -> AppConstant.OOO_NOTE_ID.ME04
        AppConstant.OOO_NOTE_CODE.ME05 -> AppConstant.OOO_NOTE_ID.ME05
        AppConstant.OOO_NOTE_CODE.ME99 -> AppConstant.OOO_NOTE_ID.ME99
        else -> 0
    }
}

fun Dialog?.setCustomWidthPopupDialog(width: Double) {
    val window = this?.window
    val size = Point()
    val display: Display

    if(window != null) {
        display = window.windowManager.defaultDisplay
        display.getSize(size)
        val maxWidth = size.x
        window.setLayout((maxWidth * width).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawableResource(R.drawable.bg_card)
        window.setGravity(Gravity.CENTER)
    }
}

fun String.generateQrCode(contex: Context?): Bitmap? {
    var bitmap: Bitmap? = null
    val manager = contex?.getSystemService(WINDOW_SERVICE) as WindowManager?
    val display = manager?.defaultDisplay
    val point = Point()
    display?.getSize(point)
    val width = point.x
    val height = point.y

    var smallerDimension = if (width < height) width else height
    smallerDimension = smallerDimension * 3 / 4

    val qrgEncoder = QRGEncoder(this, QRGContents.Type.TEXT, smallerDimension)
    qrgEncoder.colorBlack = Color.BLACK
    qrgEncoder.colorWhite = Color.WHITE

    try {
        bitmap = qrgEncoder.bitmap
    } catch (error: WriterException) {
        error.printStackTrace()
    }

    return bitmap
}