package com.bobobox.lite.external.constant

/**
 * Created by Yoga C. Pranata on 17/10/2020.
 * Android Engineer
 */
object DialogConstant {

    object DIALOG_SCREEN {
        const val ALL_STATUS = "all_status"
        const val HOTELS = "hotels"
        const val OOO_REASON = "ooo_reason"
        const val QR_CODE = "qr_code"
    }

    object REQUEST_CODE {
        const val REQ_SELECTED_HOTEL = 400
    }

}