package com.bobobox.lite.external.extension

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bobobox.lite.R
import com.bobobox.lite.domain.adapter.GeneralAdapter

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
infix fun ViewGroup.inflate(layoutResId: Int): View =
    LayoutInflater.from(context).inflate(layoutResId, this, false)

fun View.setVisibleIf(condition: Boolean) {
    visibility = if (condition) View.VISIBLE else View.GONE
}

fun ImageView.setActive(condition: Boolean) {
    if (condition) {
        setColorFilter(ContextCompat.getColor(context, R.color.blue_700))
    } else {
        setColorFilter(ContextCompat.getColor(context, R.color.grey))
    }
}

fun <ITEM> RecyclerView.setup(
    items: List<ITEM>,
    layoutResId: Int,
    bindHolder: View.(ITEM) -> Unit,
    itemClick: View.(ITEM) -> Unit = {},
    manager: RecyclerView.LayoutManager = LinearLayoutManager(this.context)
): GeneralAdapter<ITEM> {
    val generalAdapter by lazy {
        GeneralAdapter(items, layoutResId,
            {
                bindHolder(it)
            }, {
                itemClick(it)
            }
        )
    }

    layoutManager = manager
    adapter = generalAdapter
    (adapter as GeneralAdapter<*>).notifyDataSetChanged()

    return generalAdapter
}