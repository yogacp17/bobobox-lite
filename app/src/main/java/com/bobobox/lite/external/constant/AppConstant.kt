package com.bobobox.lite.external.constant

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
object AppConstant {
    const val DATABASE_NAME = "bobobox_lite_db"
    const val SPLASH_DELAY = 2000 // 2 seconds
    const val ALL_STATUS = "All Status"
    const val POPUP_DIALOG_WIDTH = 0.90
    const val ITEM_NOT_FOUND = -1
    const val DB_UPDATED = "updated"

    object USER_LOGIN {
        const val name = "Yoga C. Pranata"
        const val email = "admin@bobobox.co.id"
        const val password = "admin"
    }

    object SHARED_PREF {
        const val SHARED_NAME = "Bobobox_lite_shared"
        const val USER_LOGIN = "user_login_info"
        const val SELECTED_HOTEL = "selected_hotel"
    }

    object PROGRESSBAR {
        const val START = 0.1f
        const val MIDDLE = 0.5f
        const val FINISH = 1f
    }

    object POD_STATUS {
        const val VD = "Vacant Dirty"
        const val VC = "Vacant Clean"
        const val VCI = "Vacant Clean Inspected"
        const val O = "Occupied"
        const val OOO = "Out of Order"
        const val OUD = "OUD"
    }

    object OOO_NOTE_CODE {
        const val ME01 = "ME01"
        const val ME02 = "ME02"
        const val ME03 = "ME03"
        const val ME04 = "ME04"
        const val ME05 = "ME05"
        const val ME99 = "ME99"
    }

    object OOO_NOTE_ID {
        const val ME01 = 1000
        const val ME02 = 1001
        const val ME03 = 1002
        const val ME04 = 1003
        const val ME05 = 1004
        const val ME99 = 1099
    }

    object INTENT_BUNDLE_KEY {
        const val ROOMID = "roomId"
        const val QRCODE = "qrcode"
        const val POD_NUMBER = "pod_number"
    }
}