package com.bobobox.lite.data.model.hotel

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
data class HotelResponse(
    val code: Int,
    val status: Int,
    val message: String?,
    val data: List<Hotel>?,
    val params: List<Any>?
)