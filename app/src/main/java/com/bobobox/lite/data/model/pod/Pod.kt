package com.bobobox.lite.data.model.pod

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
data class Pod(
    val roomId: Int,
    val roomName: String,
    val roomStatus: Int,
    val description: String,
    val mqttTopic: String,
    val qrCode: String,
    val roomNotes: List<RoomNote>,
    val roomStatusCode: String,
    val roomStatusLabel: String,
    val roomType: String,
    val roomTypeLabel: String
)