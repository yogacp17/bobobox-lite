package com.bobobox.lite.data.model.pod

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
data class RoomNote(
    val noteId: Int,
    val noteCode: String,
    val noteName: String
)