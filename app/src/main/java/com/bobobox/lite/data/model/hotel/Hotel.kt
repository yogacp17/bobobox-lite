package com.bobobox.lite.data.model.hotel

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
data class Hotel(
    val id: Int,
    val latitude: String,
    val longitude: String,
    val address: String,
    val city: String,
    val email: String,
    val mqttTopic: String,
    val name: String,
    val pictureUrl: String,
    val rating: String,
    val slug: String,
    val telephone: String
)