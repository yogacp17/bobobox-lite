package com.bobobox.lite.data.model.pod

/**
 * Created by Yoga C. Pranata on 16/10/2020.
 * Android Engineer
 */
data class PodResponse(
    val code: Int,
    val status: Int,
    val data: List<Pod>?,
    val message: String?,
    val params: List<Any>?
)